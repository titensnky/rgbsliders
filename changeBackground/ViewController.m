//
//  ViewController.m
//  changeBackground
//
//  Created by Paul on 20.03.2018.
//  Copyright © 2018 Home. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UISlider *red; //interface builder
@property (weak, nonatomic) IBOutlet UISlider *green;
@property (weak, nonatomic) IBOutlet UISlider *blue;
@property (weak, nonatomic) IBOutlet UISlider *opacity;

@property (weak, nonatomic) IBOutlet UILabel *hex;

@end

@implementation ViewController

-(void) setCurrentHex {
    self.hex.text = [NSString stringWithFormat:@"#%02x%02x%02x",(unsigned int)self.red.value,(unsigned int)self.green.value,(unsigned int)self.blue.value];
}
- (IBAction)opacitySlider:(id)sender {
    const CGFloat* components = CGColorGetComponents(self.view.backgroundColor.CGColor);
    self.view.backgroundColor = [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:self.opacity.value];
}


- (IBAction)redSlider:(id)sender {
    const CGFloat* components = CGColorGetComponents(self.view.backgroundColor.CGColor);
    self.view.backgroundColor = [UIColor colorWithRed:self.red.value/255.0 green:components[1] blue:components[2] alpha:self.opacity.value];
    
    self.red.tintColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:self.red.value/255];
    
    [self setCurrentHex];
}
- (IBAction)greenSlider:(id)sender {
    const CGFloat* components = CGColorGetComponents(self.view.backgroundColor.CGColor);
    self.view.backgroundColor = [UIColor colorWithRed:components[0] green:self.green.value/255.0 blue:components[2] alpha:self.opacity.value];
    self.green.tintColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:self.green.value/255];
    [self setCurrentHex];
}
- (IBAction)blueSlider:(id)sender {
    const CGFloat* components = CGColorGetComponents(self.view.backgroundColor.CGColor);
    self.view.backgroundColor = [UIColor colorWithRed:components[0] green:components[1] blue:self.blue.value/255.0 alpha:self.opacity.value];
    self.blue.tintColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:self.blue.value/255];
    [self setCurrentHex];
}

- (void)viewDidLoad {
    [super viewDidLoad]; //newcomment
    self.view.backgroundColor = [UIColor colorWithRed:self.red.value/255.0 green:self.green.value/255.0 blue:self.blue.value/255.0 alpha:self.opacity.value];
   [self setCurrentHex];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
