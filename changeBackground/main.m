//
//  main.m
//  changeBackground
//
//  Created by Paul on 20.03.2018.
//  Copyright © 2018 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
